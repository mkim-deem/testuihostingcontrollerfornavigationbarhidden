//
//  ViewController.swift
//  TestUIHostingControllerForNavigationBarHidden
//
//  Created by MinJeong Kim on 25/11/2021.
//

import UIKit
import SwiftUI

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSwiftUIView()
    }

    func setupSwiftUIView() {
        // set navigation bar hidden true
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // instantiate UIHostingController with Swift view
        let swiftUIView = SwiftUIView()
        let hostingController = UIHostingController(rootView: swiftUIView)
        
        // add view controller
        addChild(hostingController)
        
        // add view
        let subView = hostingController.view!
        subView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subView)

        NSLayoutConstraint.activate([
            subView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            subView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            subView.topAnchor.constraint(equalTo: view.topAnchor),
            subView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
      }
}

struct SwiftUIView: View {
  var body: some View {
    Text("swift ui view")
        .navigationBarHidden(true)
  }
}


class JarvisUIHostingController<Content: View>: UIHostingController<Content> {
    
    struct Configuration {
        let isNavigationBarHidden: Bool
        let isIgnoreSafeArea: Bool
    }
    
    let configuration: JarvisUIHostingController.Configuration
    
    public init(rootView: Content, configuration: JarvisUIHostingController.Configuration) {
        self.configuration = configuration
        super.init(rootView: rootView)
        
        if configuration.isIgnoreSafeArea {
            disableSafeArea()
        }
    }

    @objc required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(configuration.isNavigationBarHidden, animated: false)
    }
    
    func disableSafeArea() {
        //
    }
}
